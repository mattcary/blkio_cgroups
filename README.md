# blkio c_group testing

This repository captures experiments performed on the effectiveness of
blkio c_groups in isolating container IOPS.

#### Status

So far I have only started playing around with the `fio` benchmarking
tool and using `pidstat` to get per-process I/O metrics (which
supposedly works by scraping information in `/proc`).

## Setup

See [`scripts/`](scripts/) for ideas on how to set things up. In
practice I find myself copy-pasting from the scripts rather than
running them independently.

They assume a project has been set with `gcloud config set project`,
and that the project has been associated with a billing account and
enabled the following services.

  * `compute.googleapis.com`
  * `containerregistry.googleapis.com`
  
Also set `compute/zone` to a reasonable default, eg `us-west2-c`.

### Instance Configuration

  * Run [`scripts/create.sh`](scripts/create.sh) to get an instance
    with an attached disk.
  *	`gcloud compute ssh blkio-test` to get into the instance.
	- sudo apt install -y emacs-nox fio sysstat
      + fio is for io testing, it appears in gcp examples. See
        [`scripts/fio_tests.sh`](scripts/fio_tests.sh)
	  + sysstat is for pidstat and similar utilities.
    - get minimal dot.emacs from [gitlab.com/carymatt/configs](gitlab.com/carymatt/configs)

## Experiments

### `pidstat` introduction and stats with 1 process

I created a VM with a 100G attached standard PD as described in
[`scripts/create.sh`](scripts/create.sh). I ran an fio write test as follows.

```
sudo fio --name=write_iops_test \
--filename=/dev/sdb --filesize=100G \
--time_based --ramp_time=2s --runtime=5m \
--ioengine=libaio --direct=1 --verify=0 --randrepeat=0 \
--bs=4K --iodepth=64 --rw=randwrite   
```

With a runtime of 5m I could casually go to a different shell, pick
out all the fio-releated process by hand with `ps -C fio` and then run

```
sudo pidstat -p 3225 -d 3
```

where the processes was the fio one without a terminal, `-d` is to
report i/o statistics, and `3` is the interval for reporting
stats. The output looked like

```
08:01:56 PM   UID       PID   kB_rd/s   kB_wr/s kB_ccwr/s iodelay  Command
08:01:59 PM     0      3475      0.00   1321.33      0.00       0  fio
08:02:02 PM     0      3475      0.00   1316.00      0.00       0  fio
08:02:05 PM     0      3475      0.00   1320.00      0.00       0  fio
```

which matched the fio job which was reporting the following.

```
Jobs: 1 (f=1): [w(1)] [43.0% done] [0KB/1320KB/0KB /s] [0/330/0 iops] [eta 02m:52s]
```

Then doing a read test like

```
sudo fio --name=read_iops_test \
--filename=/dev/sdb --filesize=100G \
--time_based --ramp_time=2s --runtime=5m \
--ioengine=libaio --direct=1 --verify=0 --randrepeat=0 \
--bs=4K --iodepth=256 --rw=randread
```

produced the following

```
08:24:49 PM   UID       PID   kB_rd/s   kB_wr/s kB_ccwr/s iodelay  Command
08:24:52 PM     0      3785    656.00      0.00      0.00       0  fio
08:24:55 PM     0      3785    657.33      0.00      0.00       0  fio
08:24:58 PM     0      3785    656.00      0.00      0.00       0  fio
08:25:01 PM     0      3785    656.00      0.00      0.00       0  fio
```

which also matched the fio output

```
Jobs: 1 (f=1): [r(1)] [21.5% done] [660KB/0KB/0KB /s] [165/0/0 iops] [eta 03m:57s]
```

Note, however, pidstat is doing byte rates and not IOPS. The ratio of
rate to IOPS for both read and writes is 4k, matching up with the
block size, but I don't know if that will hold in general.

### `pidstat` and 4 jobs with `direct=1`

Running the following random read-write test with 4 jobs like

```
sudo fio --name=rw_4jobs \
--filename=/dev/sdb --filesize=100G \
--time_based --ramp_time=2s --runtime=5m \
--ioengine=libaio --direct=1 --verify=0 --randrepeat=0 \
--bs=4K --iodepth=256 --rw=randrw --numjobs=4
```

This time I rand `pidstat` with the more clever line to grab fio
processes:

```
sudo pidstat -d -p $(ps -C fio | egrep '^ *[0-9]* \?' | cut -f 2 -d ' ' | paste -s -d ',') 3
```

which gave the following:

```
Average:      UID       PID   kB_rd/s   kB_wr/s kB_ccwr/s iodelay  Command
Average:        0      3900    113.60    113.24      0.00      77  fio
Average:        0      3901    112.62    112.27      0.00      74  fio
Average:        0      3902    117.69    108.44      0.00      79  fio
Average:        0      3904    114.04    112.36      0.00      75  fio
```

which nearly match the `fio` averages of 26 IOPS for both read and
write (=104K/s with 4k blocks).

### `direct=0` doesn't change much?

I had assumed that indirect/bufferred I/O would not be able to be
accounted for at the process level, but that doesn't seem to be the
case. The VM I'm running on has two processors. I ran a read-write
benchmark with 6 jobs and direct switched off (and buffered switched
on just in case):

```
sudo fio --name=rw_4jobs \
--filename=/dev/sdb --filesize=100G \
--time_based --ramp_time=2s --runtime=5m \
--ioengine=libaio --direct=0 --verify=0 --randrepeat=0 \
--bs=4K --iodepth=256 --rw=randrw --numjobs=6 --buffered=1
```

The benchmark was fairly steady at 20 IOPS for both read and write
during the run, except for occasional instances when it would drop to
0 for several seconds (PD or network throttling of some sort?). With
4k blocks that maps to 80 K/s, which was what `pidstat` observed.

```
Average:      UID       PID   kB_rd/s   kB_wr/s kB_ccwr/s iodelay  Command
Average:        0      5471     79.29     84.21      0.00     300  fio
Average:        0      5472     79.42     79.17      0.00     300  fio
Average:        0      5473     79.62     78.83      0.00     300  fio
Average:        0      5474     79.79     84.21      0.00     300  fio
Average:        0      5476     79.50     78.92      0.00     300  fio
Average:        0      5478     79.67     80.08      0.00     300  fio
```

This holds true even when multiple fio jobs are running, and when the
`sync` engine is used rather than `libaio` (native linux async io).

### `pidstat` with unbalanced jobs

Perhaps when all jobs are blazing away at their maximum, buffering is
easy to allocate across processes. What if one job is just writing a
small bit, and another one is red hot, can their rates still be
reported accurately?

The following fio command starts two jobs, one that runs full-on and
the other that is limited to 10 KiB. This also seemed to perform just
fine.

```
sudo fio \
--filename=/dev/sdb --filesize=100G \
--time_based --ramp_time=2s --runtime=5m \
--ioengine=sync --direct=0 --buffered=1 \
--verify=0 --randrepeat=0 \
--bs=4K --iodepth=256 --rw=randrw --name hot --name slow --rate 10KiB,10KiB
```

`pidstat` output:

```
Average:      UID       PID   kB_rd/s   kB_wr/s kB_ccwr/s iodelay  Command
Average:        0      6382    646.33    634.33      0.00     297  fio
Average:        0      6383      9.83      9.83      0.00       6  fio
```

### blkio cgroup and direct

Complete data and command lines from this section can be found in
[`data/blkio_cgroup.md`](data/blkio_cgroup.md).

I ran two fio jobs in a blkio cgroup, one with indirect I/O and the
other direct. The read stats from blkio
(`/sys/fs/cgroup/blkio/.../blkio.throttle.io_service_bytes`) match up
with the bytes fio reported pulling, but the written bytes differed by
82% (lower with indirect I/O and `buffering=1`) while the values
reported by fio were within 2%.

I had
[read](https://serverfault.com/questions/605642/why-are-cgroups-blkio-serviced-bytes-and-iotop-producing-diverging-results)
that write buffering in the kernel would cause *no* writes to be
reported within the blkio stats; that no longer seems to be the case
but there does seem to be inconsistencies in reporting.

`pidstat` displayed reasonable numbers but I don't think I could see
even a 34% difference. 

### `/proc/[pid]/io`

`pidstat` seemed to report correct numbers. I ran a rate-limited job
inside a cgroup, with a 5-job unrestricted pull in parallel. Pulling
from `/proc/[pid]/io` produced stats that matched what fio reported it
pulled. So that looks pretty good as a data source. Further details in
[the detailed notes](data/blkio_cgroup.md).
      
### Speculation and Spelunking

Digging into `pidstat` code I've found that it uses `/proc/[pid]/io`
(or `/proc/[pid]/task/[tid]/io`) to get its IO stats. While `pidstat`
just appears to use the bytes read/written fields, this file also
includes counts of read & write syscalls which could approximate I/O
operation counts.
