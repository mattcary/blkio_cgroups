# Raw(ish) data blkio cgroup reporting

## Comparing direct and indirect fio runs

I'm calling this fio run _indirect_:

```
sudo fio --filename=/dev/sdb \
  --filesize=100G --ramp_time=2s --ioengine=sync \
  --direct=0 --buffered=1 --verify=0 --randrepeat=0 \
  --bs=4K --iodepth=256 --rw=randrw --cgroup=fio \
  --size=30Mi --name cgroup_test
```

And this run _direct_:

```
sudo fio --filename=/dev/sdb \
  --filesize=100G --ramp_time=2s --ioengine=sync \
  --direct=1 --verify=0 --randrepeat=0 \
  --bs=4K --iodepth=256 --rw=randrw --cgroup=fio \
  --size=30Mi --name cgroup_test
```

Note that both of these runs are in the `fio` blkio cgroup. At the
start and end of each run, I pulled `blkio.throttle.io_service_bytes`
in both the cgroup root as well is in `fio/`.

The PD /dev/sdb is mounted at 8:16 and I've restricted the
`io_service_bytes` data to that device.

For the *direct* run:
  * fio reported 14036KB written
  * At start for root:
    - Read 807288832
    - Write 4086792192
    - Sync 3416461312
    - Async 1477619712
    - Total 4894081024
  * At end for root:
    - Read 809435136
    - Write 4086792192
    - Sync 3416461312
    - Async 1479766016
    - Total 4896227328
  * At start for `fio`:
    - Read 138276864
    - Write 80764928
    - Sync 80764928
    - Async 138276864
    - Total 219041792  
  * At end for `fio`:
	- Read 154050560
	- Write 96448512
	- Sync 96448512
	- Async 154050560
	- Total 250499072
 
For the *indirect* run:
  * fio report 13680KB  written
  * At start of root:
	- Read 805142528
	- Write 4082720768
	- Sync 3416461312
	- Async 1471401984
	- Total 4887863296
  * At end of root:
	- Read 807288832
	- Write 4086792192
	- Sync 3416461312
	- Async 1477619712
	- Total 4894081024
  * At start of `fio`:
	- Read 122593280
	- Write 69062656
	- Sync 69062656
	- Async 122593280
	- Total 191655936
  * At end of `fio`:
	- Read 138276864
	- Write 80764928
	- Sync 80764928
	- Async 138276864
	- Total 219041792

Another *indirect* run, with *buffered=1* explicitly set.
  * fio reported 13752 / 13504 KB read/write
  * start:
	- Read 184958976
	- Write 110645248
	- Sync 110645248
	- Async 184958976
	- Total 295604224
  * end:
	- Read 200896512
	- Write 119234560
	- Sync 119234560
	- Async 200896512
	- Total 320131072

Summarizing the above, for direct:
  * fio reported 14036KB written
  * Read diff root: 2146304 = 2096 KiB
  * Write diff root: 4071424 = 3976 KiB
  * Read diff `fio`: 15773696 = 15404 KiB
  * Write diff `fio`: 15683584 = 15316 KiB
  
And for indirect:
  * fio report 13680KB  written
  * Read diff root: 2146304 = 2096 KiB
  * Write diff root: 4071424 = 3976 KiB
  * Read diff `fio`: 15683584 = 15316 KiB
  * Write diff `fio`: 11702272 = 11428 KiB

Indirect with explicit buffering:
  * fio reported 13752 / 13504 KB read/write
  * Read diff `fio`: 15937536 = 15564 KiB
  * Write diff `fio`: 8589312 = 8388 KiB

The blkio numbers are clearly different for writes (in fact writes
have disappeared, I would have thought they would have aggregated
up?). But they are non-zero; an [old serverfault
thread](https://serverfault.com/questions/605642/why-are-cgroups-blkio-serviced-bytes-and-iotop-producing-diverging-results)
suggests that the blkio write stats would be zero. That is no longer
the case. The reported output for the fio runs was 14036KB for the
direct run and 13680KB for the indirect run, however, which is are 2%
different and does not match either of the blkio stats which are 34%
different at any rate (82% with buffering). The fio stats do not
include ramp-up bytes, which may explain the extra bytes recorded in
the blkio stats, but not the deficit seen in indirect writes.

## Pulling from `/proc/[pid]/io` directly

This time I wrote a [script](../scripts/scrape_fio.sh) that pulls
directly from `/proc/[pid]/io`. I ran this on a limited pull at the
same time as a full read-write pull.

`/proc/[pid]/io` reported:
```
rchar: 2578242
wchar: 2572293
syscr: 632
syscw: 629
read_bytes: 2576384
write_bytes: 2572288
cancelled_write_bytes: 0
```

The limited fio pull reported
```
   READ: io=2536KB
  WRITE: io=2584KB
```

The scraping of `/proc` is done with `sleep 0.25` which at our limited
rate and the observed accuracy of sleep means bewteen 2.5 and 10 KB
lost between the last `/proc` scrape and the process
terminating. That puts the reported `/proc` stats within error of
fio's report.

Limited fio cmd:

```
sudo fio --filename=/dev/sdb --filesize=100G --ioengine=sync --direct=0 --buffered=1 --verify=0 --randrepeat=0 --bs=4K --iodepth=256 --rw=randrw --cgroup=fio --size=5Mi --name cgroup_test --rate 10KiB,10KiB
```

Full fio cmd:

```
sudo fio \
--filename=/dev/sdb --filesize=100G \
--ioengine=sync --direct=0 --buffered=1 \
--verify=0 --randrepeat=0 \
--bs=4K --iodepth=256 --rw=randrw --size=100Mi --name hot --numjobs=5
```