#!/bin/bash

output=$1
shift
if [ -z "$output" ]; then
  echo bad output: $output
  exit 1
fi

while true; do
  if [ -z "$pid" ]; then
    pid=$(ps -C fio | egrep '^ *[0-9]* \?' | sed 's/^ *//' | cut -f 1 -d ' ')
    if [ -z "$pid" ]; then continue; fi
    if [ $(echo $pid | wc | awk '{print $2}') != 1 ]; then
      echo bad pid
      exit 1
    fi
    echo found pid $pid
  fi
  if [ ! -d /proc/$pid ]; then
    echo finished
    exit 0
  fi
  (sudo cat /proc/$pid/io) > $output
  sleep 0.25
done
  
