#/bin/bash

# I don't fill the disk in practice. This takes an hour on an n1-2
# machine and since I don't actually care about the benchmark results,
# only isolation between containers, it's not necessary.
if false; then
  sudo fio --name=fill_disk \
    --filename=/dev/sdb --filesize=100G \
    --ioengine=libaio --direct=1 --verify=0 --randrepeat=0 \
    --bs=128K --iodepth=64 --rw=randwrite
fi

# Standard write test
if false; then
  sudo fio --name=write_iops_test \
    --filename=/dev/sdb --filesize=100G \
    --time_based --ramp_time=2s --runtime=1m \
    --ioengine=libaio --direct=1 --verify=0 --randrepeat=0 \
    --bs=4K --iodepth=64 --rw=randwrite
fi

# Standard read test
if false; then
  sudo fio --name=read_iops_test \
    --filename=/dev/sdb --filesize=100G \
    --time_based --ramp_time=2s --runtime=1m \
    --ioengine=libaio --direct=1 --verify=0 --randrepeat=0 \
    --bs=4K --iodepth=64 --rw=randread
fi

#
# Tests below here use the sync library as it (may?) produce more 
# buffering behavior.
#

# rw test, not timed
if false; then
  sudo fio \
  --filename=/dev/sdb --filesize=100G \
  --ramp_time=2s \
  --ioengine=sync --direct=1 \
  --verify=0 --randrepeat=0 \
  --bs=4K --iodepth=256 --rw=randrw --size=30Mi --name rw_size_test
fi

# hot/slow test
if false; then
  sudo fio --filename=/dev/sdb --filesize=100G \
    --time_based --ramp_time=2s --runtime=5m \
    --ioengine=sync --direct=0 --buffered=1 --verify=0 \
    --randrepeat=0 --bs=4K --iodepth=256 --rw=randrw \
    --name hot --name slow --rate 10KibB,10KiB
fi

# unbuffered cgroup test. This produces different blkio stats than 
# direct=0 and buffered=0.
if false; then
  sudo fio \
  --filename=/dev/sdb --filesize=100G \
  --ramp_time=2s \
  --ioengine=sync --direct=0 --buffered=1 \
  --verify=0 --randrepeat=0 \
  --bs=4K --iodepth=256 --rw=randrw --cgroup=fio --size=30Mi --name cgroup_test
fi
