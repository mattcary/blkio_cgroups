#!/bin/bash

gcloud compute instances create blkio-test --machine-type=n1-standard-2

# The disk is created separately for easier copy/paste/rejiggering
gcloud compute disks create blkio-test-a \
	   --size 100G --type pd-standard --zone $(gcloud config get-value compute/zone)
gcloud compute instances attach-disk blkio-test --disk blkio-test-a

gcloud compute disks describe blkio-test-a --zone $(gcloud config get-value compute/zone)
